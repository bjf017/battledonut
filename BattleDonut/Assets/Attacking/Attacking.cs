﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Which state the attacking is currently in
/// </summary>
public enum AttackingState
{
    Attacking,
    NotInRange,
    NoTarget,
}

/// <summary>
/// Basic attacking overtime Script
/// AttackTarget(GameObject targetObject) - Trigger the object to attack the targetObject overtime (On clock at attackSpeed) and if the object is within range.
/// </summary>
public class Attacking : MonoBehaviour
{
    [SerializeField] private int damage = 10;
    [SerializeField] private float speed = 2f;
    [SerializeField] private float range = 5f;

    private GameObject target; // defender to attack
    /// <summary>
    /// Getter for current attack state
    /// </summary>
    public GameObject GetTarget { get { return target; } }

    private AttackingState attackingState = AttackingState.NoTarget; // Current state
    /// <summary>
    /// Getter for current attack state
    /// </summary>
    public AttackingState GetAttackingState { get { return attackingState; } }

    /// <summary>
    /// Call this function to attempt a attack/pathfind to an enemy
    /// </summary>
    public void AttackTarget(GameObject targetObject)
    {
        if (targetObject != target)
        {
            target = targetObject;
            StartCoroutine(AttackRepeat());
        }
    }

    /// <summary>
    /// Remove target and stop attacking
    /// </summary>
    public void LoseTarget()
    {
        target = null;
    }

    /// <summary>
    /// Check if the target is within range
    /// </summary>
    /// <returns></returns>
    public bool InRange()
    {
        if (target != null)
        {
            return Vector3.Distance(gameObject.transform.position, target.transform.position) < range;
        }
        else return false;
    }

    /// <summary>
    /// Called to do damage to target every x seconds 
    /// </summary>
    private IEnumerator AttackRepeat()
    {
        while (target != null)
        {
            if (InRange())
            {
                attackingState = AttackingState.Attacking;

                if (target.GetComponent<Player>())
                {
                    target.GetComponent<Player>().UpdateHealth(-damage);
                }

                yield return new WaitForSeconds(speed);
            }
            else
            {
                attackingState = AttackingState.NotInRange;
            }

            yield return new WaitForSeconds(0.1f);
        }

        if (target == null)
        {
            attackingState = AttackingState.NoTarget;
        }
    }

    // Draw sphere at attack range in inspector
    private void OnDrawGizmos()
    {
        // Draw ring around attacker at size of attackRange
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z), range);
    }
}
