﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackingControllerExample : MonoBehaviour
{
    private Attacking attacking;
    private Pathfinding pathfinding;
    private GameObject targetLocation;

    public void Awake()
    {
        attacking = gameObject.GetComponent<Attacking>();
        pathfinding = gameObject.GetComponent<Pathfinding>(); // Set pathfinding reference
    }

    public void Start()
    {
        InvokeRepeating("MoveTo", 0, 1f);
    }

    private void MoveTo()
    {
        if (targetLocation != null)
        {
            if (!attacking.InRange())
            {
                Vector3 newPos = new Vector3(targetLocation.transform.position.x, targetLocation.transform.position.y + 0.5f, targetLocation.transform.position.z);
                pathfinding.Move(newPos);
            } else
            {
                pathfinding.StopMoving();
            }
        }
    }

    public void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); // Get mouse position on screen
            if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity))
            {
                if (hit.transform.gameObject.GetComponent<PlayerController>())
                {
                    targetLocation = hit.transform.gameObject;
                    attacking.AttackTarget(hit.transform.gameObject);
                }
            }
        }

        if (targetLocation != null)
        {
            if (attacking.InRange())
            {
                pathfinding.StopMoving();
            }
        }
    }
}
