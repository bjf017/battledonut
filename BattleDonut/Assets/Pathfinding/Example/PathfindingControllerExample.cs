﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathfindingControllerExample : MonoBehaviour
{
    private Pathfinding pathFinding;
    private bool canMove = true;
    private float slowSpeed = 0.02f;
    private float fastSpeed = 0.101f;
    private float currentSpeed = 0.1f;
    [SerializeField] private Transform targetPosition;

    public void Awake()
    {
        pathFinding = gameObject.GetComponent<Pathfinding>();
    }

    public void Update()
    {
        //pathFinding.Move(targetPosition.position);
        Debug.Log(currentSpeed);

        if (canMove) {
            pathFinding.Move(targetPosition.position, currentSpeed);
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            Debug.Log("Stop");
            pathFinding.StopMoving();
            canMove = false;
        }

        if (Input.GetKeyDown(KeyCode.G))
        {
            Debug.Log("Go");
            canMove = true;
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            if (currentSpeed == slowSpeed)
            {
                currentSpeed = fastSpeed;
            } else
            {
                currentSpeed = slowSpeed;
            }
        }
    }
}
