﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Spawn in enemies that will attack the player
/// </summary>
public class Enemy_Spawner : MonoBehaviour
{
    [SerializeField] private GameObject enemyParent; // Parent object the enemies to keep the inspector tidy :-)
    [SerializeField] private List<GameObject> minions = new List<GameObject>(); // Allows for more than 1 type of enemy/minion
    [SerializeField] private float minionSpawnRate = 3f; // The rate that the enemies spawn
    [SerializeField] private bool minionsOn = true;

    void Start()
    {
        StartCoroutine(MinionsLoop());
    }

    /// <summary>
    /// Spawns in enemies and randomly puts them on the donut.
    /// </summary>
    /// <returns></returns>
    IEnumerator MinionsLoop()
    {
        while (minionsOn)
        {
            yield return new WaitForSeconds(minionSpawnRate);

            // The upwards direction needed to get the enemy on the donut
            Vector3 directionToLaunch = new Vector3(0f, 23f, 0) + (transform.forward * 3.5f);
            // Which enemy from the list to spawn
            int whichMinion = Random.Range(0, minions.Count);
            GameObject newMinion = Instantiate(minions[whichMinion], transform.position, transform.rotation);
            newMinion.transform.SetParent(enemyParent.transform);
            newMinion.GetComponent<Rigidbody>().AddForce(directionToLaunch, ForceMode.Impulse);
            // Randomly rotate spawner for next enemy spawn in
            gameObject.transform.eulerAngles = new Vector3(0, Random.Range(-360, 360), 0);
        }
    }
}
