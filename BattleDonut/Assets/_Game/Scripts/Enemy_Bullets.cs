﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Current pattern of bullet spawning
/// </summary>
public enum SpawningType
{
    basicLeft,
    basicRight,
    fastLeft,
    slowSolid,
    bulletBurst,
}

/// <summary>
/// Enemy bullet spawning. Spawning patterns of bullets in the middle of the map that can hit and damage the player.
/// </summary>
public class Enemy_Bullets : MonoBehaviour
{
    [SerializeField] private SpawningType spawningState = SpawningType.basicLeft;
    [SerializeField] private bool bulletSpawning = true;
    [SerializeField] private GameObject bullet;
    private Rigidbody rb;
    // Bullet object pool
    private Queue<GameObject> bulletPool = new Queue<GameObject>();

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        StartCoroutine(BulletLoop()); // Start bullet loop
        StartCoroutine(SwitchState()); // Start randomly switching bullet patterns
    }

    /// <summary>
    /// Spawn bullets in four different directions at once
    /// - Forward
    /// - Back
    /// - Left
    /// - Right
    /// </summary>
    private void FourDirectionSpawn()
    {
        // The directions to set end of the 4 bullets
        List<Vector3> directions = new List<Vector3>() { transform.forward, transform.right, -transform.right, -transform.forward };
        foreach (Vector3 dir in directions)
        {
            GameObject newBullet;
            // If the bullet pool is empty create a new bullet if not take the bullet out of the pool
            if (bulletPool.Count < 1)
            {
                newBullet = Instantiate(bullet, transform.position, new Quaternion(Random.Range(-180, 180), Random.Range(-180, 180), Random.Range(-180, 180), 180));
                newBullet.transform.SetParent(gameObject.transform);
                newBullet.SetActive(true);
                StartCoroutine(TrailOn(newBullet));
            }
            else
            {
                newBullet = bulletPool.Dequeue();
                newBullet.transform.position = gameObject.transform.position;
                newBullet.SetActive(true);
                StartCoroutine(TrailOn(newBullet));
            }
            newBullet.GetComponent<Rigidbody>().AddForce(dir * 500);
            StartCoroutine(DestroyBullet(newBullet));
        }
    }

    /// <summary>
    /// Turns on the bullet trail again after it has been spawned or taken out of the bullet pool
    /// </summary>
    /// <param name="newBullet">The bullet to turn the trail on for</param>
    IEnumerator TrailOn(GameObject newBullet)
    {
        yield return new WaitForSeconds(0.5f);
        newBullet.GetComponent<TrailRenderer>().enabled = true;
    }

    /// <summary>
    /// Add the bullet to object pool after delay of the bullet being spawned
    /// </summary>
    /// <param name="deadBullet">The bullet to add back to the object pool</param>
    /// <returns></returns>
    public IEnumerator DestroyBullet(GameObject deadBullet)
    {
        yield return new WaitForSeconds(3f);

        // Needs to have null check incase the bullet was destroyed from hitting the player.
        if (deadBullet != null)
        {
            deadBullet.GetComponent<TrailRenderer>().enabled = false;
        }

        yield return new WaitForSeconds(2f);

        if (deadBullet != null)
        {
            deadBullet.GetComponent<Rigidbody>().velocity = Vector3.zero;
            deadBullet.SetActive(false);
            bulletPool.Enqueue(deadBullet);
        }
    }

    /// <summary>
    /// Randomly change bullet patterns every 5 to 15 seconds
    /// </summary>
    IEnumerator SwitchState()
    {
        // Looping while bullet spawning is turned on
        while (bulletSpawning)
        {
            float randomTime = Random.Range(5, 15);
            yield return new WaitForSeconds(randomTime);

            SpawningType curState = spawningState;
            SpawningType newState = spawningState;

            while (curState == newState)
            {
                newState = (SpawningType)Random.Range(0, 5);
            }
            spawningState = newState;
        }
    }

    /// <summary>
    /// Loop between spawning each bullet at a rate depending on the current bullet pattern
    /// </summary>
    IEnumerator BulletLoop()
    {
        while (bulletSpawning)
        {
            switch (spawningState)
            {
                case SpawningType.basicLeft:
                    yield return new WaitForSeconds(0.5f);
                    rb.angularVelocity = new Vector3(0, 0.4f, 0);
                    FourDirectionSpawn();
                    break;

                case SpawningType.basicRight:
                    yield return new WaitForSeconds(0.5f);
                    rb.angularVelocity = new Vector3(0, -0.4f, 0);
                    FourDirectionSpawn();
                    break;

                case SpawningType.fastLeft:
                    yield return new WaitForSeconds(0.5f);
                    rb.angularVelocity = new Vector3(0, 1f, 0);
                    FourDirectionSpawn();
                    break;

                case SpawningType.slowSolid:
                    yield return new WaitForSeconds(0.2f);
                    rb.angularVelocity = new Vector3(0, 0.3f, 0);
                    FourDirectionSpawn();
                    break;

                case SpawningType.bulletBurst:
                    yield return new WaitForSeconds(0.4f);
                    rb.angularVelocity = new Vector3(0, 0.3f, 0);
                    for (int i = 0; i < 4; i++)
                    {
                        yield return new WaitForSeconds(0.1f);
                        FourDirectionSpawn();
                    }
                    break;
            }
        }
    }
}
