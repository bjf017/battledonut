﻿using UnityEngine;

/// <summary>
/// Type of pickup
/// </summary>
public enum PickupType
{
    energy,
    health,
    super,
}

/// <summary>
/// Handles all types of pickups
/// </summary>
public class Pickup : MonoBehaviour
{
    public PickupType pickupType = PickupType.energy;
    [SerializeField] private float amount = 30;

    private float timer = 0f;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Invoke("DestroyPickup", 20f);
    }

    void Update()
    {
        timer += Time.deltaTime;
        if (timer > 0.5f)
        {
            timer = 0;
            rb.angularVelocity += new Vector3(Random.Range(0, 10), Random.Range(0, 10), Random.Range(0, 10));
        }
    }

    /// <summary>
    /// Returns the value of the pickup
    /// - How much health to give or take
    /// - How much energy to give or take
    /// - Or if it is a super pickup 
    /// </summary>
    /// <returns>The value of the pickup</returns>
    public float GetValue()
    {
        switch (pickupType)
        {
            case PickupType.energy:
                return amount;

            case PickupType.health:
                return amount;

            case PickupType.super:
                return Mathf.PI;

            default:
                return amount;
        }
    }

    /// <summary>
    /// Make the pickup fall through the ground if it is not used. 
    /// </summary>
    public void DestroyPickup()
    {
        rb.useGravity = true;
        Invoke("DestroyObject", 1f);
    }

    /// <summary>
    /// Destroy the pickup
    /// </summary>
    public void DestroyObject()
    {
        Destroy(gameObject);
    }
}
