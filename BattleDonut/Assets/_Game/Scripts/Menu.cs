﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Controls the basic inputs and start up functions of the main menu
/// </summary>
public class Menu : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI highscore;

    public void Start()
    {
        // Display highscore from previous plays
        TimeSpan highscoreTime = TimeSpan.FromSeconds(gameObject.GetComponent<ScoreSystem>().GetHighScore());
        highscore.text = highscoreTime.ToString(@"hh\:mm\:ss");
    }

    /// <summary>
    /// Starts up the game to play
    /// </summary>
    public void Button_Start()
    {
        SceneManager.LoadScene("Map");
    }

    /// <summary>
    /// Close the game
    /// </summary>
    public void Button_Quit()
    {
        Application.Quit();
    }
}
