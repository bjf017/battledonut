﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Handles everything for the player, including
/// - UI
/// - Movement
/// - Stats
/// - Shooting
/// - Camera
/// - Pickups
/// </summary>
public class Player : MonoBehaviour
{
    // CHEATS
    [Header("Cheats")]
    [SerializeField] private bool godMode = false;
    [SerializeField] private bool infiniteAmmo = false;

    // PLAYER REFERENCE
    [Header("Player Stats")]
    [Space(5)]
    [SerializeField] private int timer = 0;
    [SerializeField] private int health = 100;
    [SerializeField] private int energy = 10;
    [SerializeField] private float movementSpeed = 5;

    [Header("References")]
    [SerializeField] private GameObject gun;
    [SerializeField] private Transform bulletLocation;
    [SerializeField] private GameObject bulletPrefab;
    [Space(5)]
    [SerializeField] private Particle deathParticles;
    [Space(5)]
    [SerializeField] private GameObject playerCamera;
    [Space(5)]
    [SerializeField] private PostProcessVolume postProcess;

    // TIMER UI ELEMENTS
    [Header("UI")]
    [Space(5)]
    [SerializeField] private TextMeshProUGUI timerText;

    // DEATH UI ELEMENTS
    [Space(5)]
    [SerializeField] private GameObject deathUI;

    // HIGHSCORE UI ELEMENTS
    [Space(5)]
    [SerializeField] private GameObject highScoreObject;
    [SerializeField] private GameObject newHighScoreObject;
    [SerializeField] private TextMeshProUGUI highscoreText;

    // PAUSE UI ELEMENTS
    [Space(5)]
    [SerializeField] public GameObject pauseUI;

    // HEATH ELEMENTS
    [Space(5)]
    [SerializeField] private Slider healthSlider;
    [SerializeField] private TextMeshProUGUI healthText;

    // ENERGY ELEMENTS
    [Space(5)]
    [SerializeField] private Slider energySlider;
    [SerializeField] private TextMeshProUGUI energyText;

    // Private variables
    private Rigidbody rb; // Player Rigidbody
    private Vignette vig; // Camera vignette
    private float superTimer = 0;

    /// <summary>
    /// Runs on scene start
    /// </summary>
    void Start()
    {
        StartCoroutine(Clock());
        
        rb = gameObject.GetComponent<Rigidbody>();
        postProcess.profile.TryGetSettings(out vig);
    }

    private void Update()
    {
        // Makes the camera follow the player
        playerCamera.transform.LookAt(gameObject.transform);

        // Update the gun position and rotation
        gun.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 0.35f, gameObject.transform.position.z);
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hit))
        {
            Vector3 direction = new Vector3(hit.point.x, gun.transform.position.y, hit.point.z) - gun.transform.position; // Direction to move in Normalized
            gun.transform.rotation = Quaternion.Slerp(gun.transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * 5); // Smooth rotation between current rotation and new direction
        }
    }

    /// <summary>
    /// Handles collisions with Pickups and Enemy Bullets
    /// </summary>
    private void OnTriggerEnter(Collider other)
    {
        // Handles when the character gets a pickup
        if (other.gameObject.GetComponent<Pickup>())
        {
            Pickup pickupScript = other.gameObject.GetComponent<Pickup>();
            int newValue = (int)pickupScript.GetValue();

            switch (pickupScript.pickupType)
            {
                //Energy
                case PickupType.energy:
                    UpdateEnergy(newValue);
                    break;

                //Health
                case PickupType.health:
                    UpdateHealth(newValue);
                    break;

                //SuperMode
                case PickupType.super:
                    StartCoroutine(SuperPickup(3f));
                    break;
            }

            pickupScript.DestroyPickup();
        }

        // Handles if the player is hit by and enemy bullet
        if (other.tag == "EnemyBullet")
        {
            UpdateHealth(-25);
            Destroy(other.gameObject);
        }
    }
    
    /// <summary>
    /// Trigger this to put the player into super mode.
    /// Faster and endless stream of bullets for x time.
    /// </summary>
    /// <param name="time">Time that the super mode stays on</param>
    /// <returns></returns>
    IEnumerator SuperPickup(float time)
    {
        superTimer = +time;
        if (vig.enabled == false)
        {
            vig.enabled.value = true;

            while (superTimer > 0)
            {
                yield return new WaitForSeconds(0.1f);
                superTimer = superTimer - 0.1f;
                Shoot();
            }

            vig.enabled.value = false;
        }
    }

    /// <summary>
    /// Change velocity of the player
    /// </summary>
    /// <param name="newVelocity">new direction to move player</param>
    public void Move(Vector3 newVelocity)
    {
        rb.velocity = (newVelocity + (Vector3.down * 2)) * movementSpeed;
    }

    /// <summary>
    /// Spawn bullet and launch in forward direction
    /// </summary>
    public void Shoot()
    {
        if (energy > 0 || vig.enabled == true)
        {
            GameObject newBullet = Instantiate(bulletPrefab, bulletLocation.position, new Quaternion(UnityEngine.Random.Range(-180, 180), UnityEngine.Random.Range(-180, 180), UnityEngine.Random.Range(-180, 180), 180));
            newBullet.GetComponent<Rigidbody>().AddForce(bulletLocation.forward * 1000);
            StartCoroutine(DestroyPlayerBullet(newBullet));

            if (vig.enabled == false)
            {
                UpdateEnergy(-1);
            }
        }
    }

    /// <summary>
    /// Destroy the player bullet after delay
    /// </summary>
    /// <param name="bullet">bullet to destroy</param>
    /// <returns></returns>
    IEnumerator DestroyPlayerBullet(GameObject bullet)
    {
        yield return new WaitForSeconds(3f);
        Destroy(bullet);
    }

    /// <summary>
    /// Updates clock timer and UI every 1sec
    /// </summary>
    private IEnumerator Clock()
    {
        while(true)
        {
            yield return new WaitForSeconds(1f);
            TimeSpan time = TimeSpan.FromSeconds(timer);
            timerText.text = time.ToString(@"hh\:mm\:ss");
            timer++;
        }
    }

    /// <summary>
    /// Updates the energy of the player
    /// </summary>
    /// <param name="amount">The amount to decrease or increase the player energy</param>
    public void UpdateEnergy(int amount)
    {
        if (infiniteAmmo == false)
        {
            energy += amount;
        }

        if (energy > 10)
        {
            energy = 10;
        }

        energySlider.value = energy;
        energyText.text = "" + energy;
    }

    /// <summary>
    /// Updates the health of the player
    /// </summary>
    /// <param name="amount">The amount to decrease or increase the player health</param>
    public void UpdateHealth(int amount)
    {
        if (godMode == false)
        {
            health += amount;
        }

        if (health > 100)
        {
            health = 100;
        }
            
        if (health < 1)
        {
            deathParticles.SpawnParticle(gameObject.transform.position);
            PlayerDeath();
        }

        healthSlider.value = health;
        healthText.text = "" + health;
    }

    /// <summary>
    /// Activate the death UI and name input
    /// </summary>
    public void PlayerDeath()
    {
        Time.timeScale = 0;
        bool newHighScore = gameObject.GetComponent<ScoreSystem>().SetLastScore(timer);
        if (newHighScore)
        {
            newHighScoreObject.SetActive(true);
        }
        TimeSpan highscoreTime = TimeSpan.FromSeconds(gameObject.GetComponent<ScoreSystem>().GetHighScore());
        highscoreText.text = highscoreTime.ToString(@"hh\:mm\:ss");
        highScoreObject.SetActive(true);
        deathUI.SetActive(true);
    }

    /// <summary>
    /// Activate and Deactivate the pause menu
    /// </summary>
    public void Pause()
    {
        if (pauseUI.activeSelf == false)
        {
            Time.timeScale = 0;
            highscoreText.text = gameObject.GetComponent<ScoreSystem>().GetHighScore().ToString();
            highScoreObject.SetActive(true);
            pauseUI.SetActive(true);
        }
        else if (pauseUI.activeSelf == true)
        {
            Time.timeScale = 1;
            highScoreObject.SetActive(false);
            pauseUI.SetActive(false);
        }
    }

    // ---------------------------------------------------
    //                      UI ELEMENTS 
    // ---------------------------------------------------
    /// <summary>
    /// Reset the level to try again
    /// </summary>
    public void Button_Retry()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    /// <summary>
    /// Take the player to the main menu of the game
    /// </summary>
    public void Button_Menu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Menu");
    }

    /// <summary>
    /// Return back to game from pause menu
    /// </summary>
    public void Button_Back()
    {
        Pause();
    }

    // ---------------------------------------------------
    //                  END OF UI ELEMENTS 
    // ---------------------------------------------------
}
