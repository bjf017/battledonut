﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Enemy AI to control attacking and moving
/// </summary>
public class Enemy : MonoBehaviour
{
    // Pickups that can spawn on enemy death
    [SerializeField] private GameObject[] pickups = new GameObject[0];

    // Private Variables
    private Pathfinding pathfinding;
    private Attacking attacking;
    private GameObject player;
    private Particle deathParticles;

    private void Start()
    {
        player = GameObject.Find("PlayerBall");
        deathParticles = GameObject.Find("DeathParticles").GetComponent<Particle>();
        pathfinding = GetComponent<Pathfinding>();
        attacking = GetComponent<Attacking>();
        attacking.AttackTarget(player);
    }
    private void Update()
    {
        // Move towards the player
        pathfinding.Move(player.transform.position);
    }

    public void OnTriggerEnter(Collider other)
    {
        // Destroys the enemy if it is hit by a players bullet
        if (other.tag == "PlayerBullet")
        {
            // Spawn pickup
            Instantiate(pickups[Random.Range(0, pickups.Length)], new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 1, gameObject.transform.position.z), gameObject.transform.rotation);
            // Spawn death particles
            deathParticles.SpawnParticle(gameObject.transform.position);
            // Destroy the bullet that hit the enemy
            Destroy(other.gameObject);
            // Destroy the enemy
            Destroy(gameObject);
        }
    }
}
