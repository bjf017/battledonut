﻿using UnityEngine;

/// <summary>
/// Handles player inputs to then pass onto the main player script
/// </summary>
public class PlayerController : MonoBehaviour
{
    private Transform cam; // Current camera ref
    private Player playerScript; // Ref to main player script

    private void Start()
    {
        cam = Camera.main.transform.parent;
        playerScript = GetComponent<Player>();
    }

    void FixedUpdate()
    {
        // Movement
        Vector3 movementDirection = Vector3.zero;
        if (Input.GetAxis("Vertical") > 0)
        {
            movementDirection -= cam.forward;
        }
            
        if (Input.GetAxis("Vertical") < 0)
        {
            movementDirection += cam.forward;
        }
            
        if (Input.GetAxis("Horizontal") > 0)
        {
            movementDirection -= cam.right;
        }
            
        if (Input.GetAxis("Horizontal") < 0)
        {
            movementDirection += cam.right;
        }
        playerScript.Move(movementDirection);

        // Shooting
        if (Input.GetButtonDown("Fire"))
            playerScript.Shoot();

        // Pause
        if (Input.GetButtonDown("Escape"))
            playerScript.Pause();
    }
}
