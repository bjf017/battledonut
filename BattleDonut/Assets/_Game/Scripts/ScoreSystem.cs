﻿using UnityEngine;

/// <summary>
/// Score handling system to set and get the current highscore.
/// </summary>
public class ScoreSystem : MonoBehaviour
{
    /// <summary>
    /// Sets the score and updates highscore if the score is better
    /// </summary>
    /// <param name="newScore">returns true if the highscore was updated</param>
    /// <returns></returns>
    public bool SetLastScore(int newScore)
    {
        PlayerPrefs.SetInt("LastScore", newScore);

        if (newScore > GetHighScore())
        {
            PlayerPrefs.SetInt("HighScore", newScore);
            return true;
        } else
        {
            return false;
        }
    }

    /// <summary>
    /// Returns the last score that was added
    /// </summary>
    /// <returns>Returns the last score that was added</returns>
    public int GetLastScore()
    {
        return PlayerPrefs.GetInt("LastScore", 0);
    }

    /// <summary>
    /// Returns the current highscore
    /// </summary>
    /// <returns>Returns the current highscore</returns>
    public int GetHighScore()
    {
        return PlayerPrefs.GetInt("HighScore", 0);
    }
}
